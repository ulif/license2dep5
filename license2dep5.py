#!/usr/bin/python3
##
## license2dep5.py
## (c) 2022 ulif <uli@gnufix.de>
## License: GPL-3.0-or-later
##
## license2dep5 eats license files (text) and prints it wrapped to 79 chars,
## with a leading space and empty lines replaced by ".".
##
## So, "foo bar\n\nbaz\nqwe\n" becomes " foo bar\n .\n baz\n qwe\n"
##
## Licenses formatted this way can be included in copyright notices following
## DEP5 (https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/)
##
import re
import sys
import textwrap

if len(sys.argv) < 2:
    print("must give an input file.")
    sys.exit(1)


infile = sys.argv[1]
for line in open(infile):
    if line == "\n":
        line = "." + line
    prefix = re.match("^(\s*)", line).group(0)
    wrapper = textwrap.TextWrapper(
        width=79, initial_indent=" ", subsequent_indent=" " + prefix
    )
    for subline in wrapper.wrap(line):
        print(subline)
