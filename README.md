# license2dep5

## Why?

When formatting copyright files following DEP5 standard, included license files
should follow a special format:

Original:

    The Great License
    
    This is some paragraph.

Wanted:

     The Great License
     .
     This is some paragraph.

The differences:

- Text should be indented by at least one whitespace

- Empty lines should contain at least a single dot.

This little script reformats input files in this way. Texts are wrapped to
contain not more than 78 chars per line, are not empty and start with a single
space.


## How?

Check it out, then...

    $ python3 license2dep5 the-great-license.txt

